// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUi from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import * as d3 from 'd3'
import './style.css'
import source from '@/data/data.csv'
import * as VueGoogleMaps from 'vue2-google-maps'
import GmapCluster from 'vue2-google-maps/dist/components/cluster'

Vue.component('GmapCluster', GmapCluster)
window.DB = source
window.d3 = d3

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCNbl8yog1nY6uRrtlmcr1DRwKVV7oeJ5o',
    libraries: 'places',
    disableDefaultUI: true,
    language: 'en'
  }
})

Vue.use(ElementUi)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
