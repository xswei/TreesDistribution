import Vue from 'vue'
import Router from 'vue-router'
import PieBubble from '@/components/pie-bubble-com.vue'
import MapCom from '@/components/map-com.vue'
import LineCom from '@/components/line-com.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/pie',
      name: 'PieBubble',
      component: PieBubble
    },
    {
      path: '/line',
      name: 'LineCom',
      component: LineCom
    },
    {
      path: '/',
      name: 'MapCom',
      component: MapCom
    }
  ]
})
